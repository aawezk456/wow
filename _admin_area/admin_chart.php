<?php
// if( $_SESSION['role'] == 'admin' )
// {
    #UnAuthorized members
    $query = "SELECT * FROM members WHERE prime = '0'";
    $get_unauth_alumnis = mysqli_query( $connection , $query );
    $unauth_alumnis = mysqli_num_rows($get_unauth_alumnis);

    #Authorized Prime
    // $query = "SELECT * FROM members WHERE prime = '1'";
    // $get_auth_stud = mysqli_query( $connection , $query );
    // $auth_stud = mysqli_num_rows($get_auth_stud);  

    #Authorized Alumnis
    $query = "SELECT * FROM members WHERE prime = '1'";
    $get_auth_alumnis = mysqli_query( $connection , $query );
    $auth_alumnis = mysqli_num_rows($get_auth_alumnis);

    #Unauthorized Students
    $query = "SELECT * FROM members "; //students third column
    $get_unauth_stud = mysqli_query( $connection , $query );
    $totalmembers = mysqli_num_rows($get_unauth_stud);
 
    #Array Collection
    $Data_Name = [ 'Members' , 'Prime Members' , 'Total Members' ];
    $Authorized_Val = [$auth_alumnis  ];
    $Unauthorized_Val = [$unauth_alumnis  ];
    $total_members = [$totalmembers];
// }
?>
   

<script type="text/javascript" src="./js/loader.js"></script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['bar']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Activity'  , 'Prime Members' , 'Members' ,'Total Members'],

      <?php

            for( $i=0; $i<1; $i++ )
            {
                echo " [ '{$Data_Name[$i]}' " . "," . "{$Authorized_Val[$i]}" . "," . "{$Unauthorized_Val[$i]} "." ,"."{$total_members[$i]} ] " ;
            }

      ?>
            
    ]);

    var options = {
      chart: {
        title: 'Overall Activity',
        subtitle: 'Members , Prime Members ,Total Members ',
      }
    };

    var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

    chart.draw(data, google.charts.Bar.convertOptions(options));
  }
</script>
<div id="columnchart_material" style="width: 'auto'; height: 500px;"></div>