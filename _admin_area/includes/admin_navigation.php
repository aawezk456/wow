<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
<!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php" style=" color:white; ">Admins</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li><a href="../index.php">Home Page</a></li>
        <!--Dump messages.php-->
        <?php #include"..//dump/messages.php"; ?>
        <!--Dump alert.php-->
        <?php #include"..//dump/alertmsg.php"; ?>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $admin_name; ?> <b class="caret"></b></a>
            <ul class="dropdown-menu">
            <li>
            <a href="admin_profile.php" ><i class="fa fa-fw fa-user"></i> Profile</a>
                </li>
                <!-- <li class="divider">HR in LI</li> -->
                <li>
                    <a href="../Process/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li>
                <a href="index.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
           
        
            <li>
                <a href="#"><i class="fa fa-fw fa-envelope"></i> Send Message </a>
            </li>
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#users_dropdown"><i class="fa fa-fw fa-users"></i> Members <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="users_dropdown" class="collapse">
                    <li>
                        <a href="./members.php">View Members</a>
                    </li>
                    <li>
                        <a href="./prime_members.php">View Prime members</a>
                    </li>
                    <li>
                        <a href="./nonprime.php">View Non-Prime members</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
<!-- /.navbar-collapse -->
</nav>