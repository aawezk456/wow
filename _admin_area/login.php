<?php

session_start();

include("includes/db_conn.php");

?>
<!DOCTYPE HTML>
<html>

<head>

<title>Admin Login</title>

<link rel="stylesheet" href="css/bootstrap.min.css" >

<link rel="stylesheet" href="css/login.css" >

</head>

<body>

<div class="container" ><!-- container Starts -->

<form class="form-login" action="" method="post" ><!-- form-login Starts -->

<h2 class="form-login-heading" >Admin Login</h2>

<input type="text" class="form-control" name="admin_phone" placeholder="Phone Number" required >

<input type="password" class="form-control" name="admin_pass" placeholder="Password" required >

<button class="btn btn-lg btn-primary btn-block" type="submit" name="admin_login" >

Log in

</button>


</form><!-- form-login Ends -->

</div><!-- container Ends -->



</body>

</html>
<?php

if(isset($_POST['admin_login'])){

$admin_phone = mysqli_real_escape_string($connection,$_POST['admin_phone']);

$admin_pass = mysqli_real_escape_string($connection,$_POST['admin_pass']);

$get_admin = "select * from admins where admin_phone='$admin_phone' AND admin_pass='$admin_pass'";



$run_admin = mysqli_query($connection,$get_admin);

  $count = mysqli_num_rows($run_admin);

if($count==1){

$_SESSION['admin_phone']=$admin_phone;


echo "<script>alert('You are Logged in into admin panel')</script>";

echo "<script>window.open('index.php','_self')</script>";

}
else {

echo "<script>alert('Phone or Password is Wrong')</script>";
echo "<script>window.open('login.php','_self')</script>";
// header('location:login.php');


}

}

?>