<!doctype html>
<?php include'includes/_header.php'; ?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>WebOfWallet</title>
</head>

<body style="background-color:#b7ddff;">


    <div class="title text-center wow fadeIn" data-wow-duration="500ms">
        <h2>Registration</h2>

        <div class="border "></div>
        <div class="contact-info  wow fadeInUp" data-wow-duration="500ms"></div>
        <div class="container col-md-4">
            <div class="contact-form  wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
                <br><br>
                <form id="contact-form" method="post" action="Process/send_otp.php" role="form">

                    <div class="form-group">
                        <p>
                            <h5 class="text-left">Referal Code:</h5>
                        </p>
                        <input type="text" placeholder="Mobile No."
                            onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" minlength="10"
                            maxlength="10" class="form-control" name="refer_code" id="refer_code" required>
                    </div>

                    <div class="form-group">
                        <p>
                            <h5 class="text-left">Username:</h5>
                        </p>
                        <input type="text" placeholder="Mobile No."
                            onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;" minlength="10"
                            maxlength="10" class="form-control" name="username" id="username" required>
                    </div>


                    <div class="form-group">
                        <p>
                            <h5 class="text-left">Password:</h5>
                        </p>
                        <input type="password" placeholder="Password" class="form-control" name="password" id="password"
                            required>
                    </div>
                    <div class="form-group">
                        <p>
                            <h5 class="text-left">Confirm Password:</h5>
                        </p>
                        <input type="password" placeholder="Re-Enter Password" class="form-control" name="confirmpw"
                            id="confirmpw" required>
                    </div>

                    <div class="form-group"><br>
                        <p>
                            <h5 class="text-center">I agree to the <a href="#">Terms & Conditions</a></h5>
                        </p><br>
                    </div>

                    <div id="cf-submit">
                        <button type="submit" id="contact-submit" name="send_otp" class="btn btn-transparent">
                            REGISTER
                        </button>
                    </div>

                </form>
                <br>
                <p>
                    <h4 class="text-center">Already Have an Account? <a href="login.php">Login</a></h4>
                </p>
            </div>
        </div>

    </div>




    <?php 
    include'includes/_footer.php';
     
    
     
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <script>
    $(document).ready(function() {
        $('#contact-submit').click(function(event) {

            data = $('#password').val();
            var len = data.length;

            if (len < 1) {
                alert("Fields cannot be blank");
                // Prevent form submission
                event.preventDefault();
            }

            if ($('#password').val() != $('#confirmpw').val()) {
                alert("Password and Confirm Password don't match");
                // Prevent form submission
                event.preventDefault();
            }

        });




    });
    </script>


</body>

</html>